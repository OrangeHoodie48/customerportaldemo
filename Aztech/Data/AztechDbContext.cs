﻿using Aztech.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Data
{
    public class AztechDbContext : IdentityDbContext<IdentityModel>
    {
        public AztechDbContext(DbContextOptions<AztechDbContext> options): base(options)
        {

        }

        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<OpenPartOrder> OpenPartOrders { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<ShippedPartOrder> ShippedPartOrders { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Quote> Quotes { get; set; } 
        public DbSet<PartModel> PartModels { get; set; }
    }
}
