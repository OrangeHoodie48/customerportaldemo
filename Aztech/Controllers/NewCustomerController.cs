﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Aztech.Data;
using Aztech.Models;
using Aztech.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Aztech.Controllers
{
    [Authorize]
    public class NewCustomerController : Controller
    {
        IDataServices dataServices;
        PasswordGen passGen; 
        UserManager<IdentityModel> userManager;
        public NewCustomerController(IDataServices dataServices, UserManager<IdentityModel> userManager, PasswordGen passGen)
        {
            this.dataServices = dataServices;
            this.userManager = userManager;
            this.passGen = passGen; 
        }

        [HttpGet]
        public async Task<IActionResult> AddCustomer()
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority < 2)
            {
                return RedirectToAction("Index", "Home");
            }

            AppUser customer = new AppUser();
            customer.Type = "Customer";
            return View(customer);
        }

        [AutoValidateAntiforgeryToken]
        [HttpPost]
        public async Task<IActionResult> AddCustomer(AppUser customer)
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority < 2)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View(customer);
            }
            customer.FirstLogin = "N/A";
            if(dataServices.AddAppUser(customer) != true)
            {
                throw new Exception("Failed to create customer model");
            }
            IdentityModel iUser = new IdentityModel();
            iUser.Email = customer.Email;
            iUser.AppUserId = customer.AppUserId;
            iUser.UserName = customer.Email;
            iUser.SimulationCode = -1;
            iUser.UserAuthority = 1;

            customer.Password = passGen.GetPassword(); 
            var result = await userManager.CreateAsync(iUser, customer.Password);
            if (!result.Succeeded)
            {
                if(dataServices.DeleteAppUser(customer) != true)
                {
                    throw new Exception("Faileed to both create Identity User and to delete customer model");
                }
                throw new Exception("Failed to create user"); 
            }


            string body = "Hello an account has been created for you. Use this email and the following temporary password to log in" + 
                          "It is highly reccomended that you change your password upon your first log in." + 
                           "   Temporary Password: " + customer.Password;
            var smtpClient = new SmtpClient
            {
                Host = "smtp.aol.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential("stevend39addario@aol.com", "C1c3r0Th3Th1rd")
            };
            using (var message = new MailMessage("stevend39addario@aol.com", customer.Email) { Subject = "Account Creation", Body = body })
            {
                smtpClient.Send(message);
            }
            return RedirectToAction("Index", "Home"); 
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult CustomerCreate()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            AppUser customer = new AppUser();
            customer.Type = "Customer";
            return View(customer);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CustomerCreate(AppUser customer)
        {
            if (!ModelState.IsValid)
            {
                return View(customer);
            }
            customer.FirstLogin = "N/A";
            if (dataServices.AddAppUser(customer) != true)
            {
                throw new Exception("Failed to create customer model");
            }
            IdentityModel iUser = new IdentityModel();
            iUser.Email = customer.Email;
            iUser.AppUserId = customer.AppUserId;
            iUser.UserName = customer.Email;
            iUser.SimulationCode = -1;
            iUser.UserAuthority = 1;

            customer.Password = passGen.GetPassword();
            var result = await userManager.CreateAsync(iUser, customer.Password);
            if (!result.Succeeded)
            {
                if (dataServices.DeleteAppUser(customer) != true)
                {
                    throw new Exception("Faileed to both create Identity User and to delete customer model");
                }
                throw new Exception("Failed to create user");
            }


            string body = "Hello an account has been created for you. Use this email and the following temporary password to log in" +
                          "It is highly reccomended that you change your password upon your first log in." +
                           "   Temporary Password: " + customer.Password;
            var smtpClient = new SmtpClient
            {
                Host = "smtp.aol.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential("stevend39addario@aol.com", "C1c3r0Th3Th1rd")
            };
            using (var message = new MailMessage("stevend39addario@aol.com", customer.Email) { Subject = "Account Creation", Body = body })
            {
                smtpClient.Send(message);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
