﻿using Aztech.Models;
using Aztech.Services;
using Aztech.ViewModels.CustomerTables;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        IDataServices dataServices;
        UserManager<IdentityModel> userManager;
        public HomeController(IDataServices dataServices, UserManager<IdentityModel> userManager)
        {
            this.dataServices = dataServices;
            this.userManager = userManager; 
        }

        public async Task<IActionResult> Index()
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority < 2)
            {
                return RedirectToAction("OpenOrdersTable", "CustomerTables", new { user.AppUserId });
            }
            else if(user.SimulationCode != -1)
            {
                return RedirectToAction("OpenOrdersTable", "CustomerTables", new { appUserId = user.SimulationCode });
            }

            List<AppUser> customers = dataServices.GetAppUsers().Where(r => r.Type.ToUpper().Equals("CUSTOMER")).ToList();
            return View(customers); 
        }

        public IActionResult CustomerIndex()
        {
            return RedirectToAction("ModifyCustomerAccount", "CustomerModifyAccount");
        }
    }
}
