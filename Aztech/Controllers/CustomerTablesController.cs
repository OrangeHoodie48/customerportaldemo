﻿using Aztech.Models;
using Aztech.Services;
using Aztech.ViewModels.CustomerTables;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Controllers
{
    [Authorize]
    public class CustomerTablesController : Controller
    {
        IDataServices dataServices;
        public CustomerTablesController(IDataServices dataServices)
        {
            this.dataServices = dataServices;
        }

        public IActionResult OpenOrdersTable(int appUserId)
        {
            OpenOrdersTableViewModel model = new OpenOrdersTableViewModel();
            model.CustomerProfile = dataServices.GetAppUsers().Where(r => r.AppUserId == appUserId).ToList()[0];
            model.AllOrders = dataServices.GetCustomerOpenPartOrders(appUserId);
            return View(model);
        }

        public IActionResult ShipmentsTable(int appUserId)
        {
            ShipmentsTableViewModel model = new ShipmentsTableViewModel();
            model.AppUserId = appUserId;
            model.Shipments = dataServices.GetCustomerShipments(appUserId);
            model.ShippedPartsByShipmentId = dataServices.GetShippedPartsGroupedByShipment(appUserId);
            return View(model);
            
        }

        public IActionResult InvoicesTable(int appUserId)
        {
            InvoicesTableViewModel model = new InvoicesTableViewModel();
            model.AppUserId = appUserId;
            model.Invoices = dataServices.GetInvoicesFor(appUserId);
            return View(model);
        }

        public IActionResult PartsTable(int appUserId)
        {
            PartsTableViewModel model = new PartsTableViewModel();
            model.AppUserId = appUserId;
            model.parts = dataServices.GetPartsForUser(appUserId);
            return View(model);
        }

        public IActionResult QuotesTable(int appUserId)
        {
            QuotesTableViewModel model = new QuotesTableViewModel();
            model.Quotes = dataServices.GetQuotesForUser(appUserId);
            model.AppUserId = appUserId;
            return View(model);
        }

        public IActionResult InvoicePdf(int invoiceId)
        {
            Invoice invoice = dataServices.GetInvoice(invoiceId);
            byte[] pdfBinary; 
           
            using (MemoryStream mem = new MemoryStream())
            {
                Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
                PdfWriter wri = PdfWriter.GetInstance(doc, mem);
                Paragraph invoiceNum = new Paragraph("Invoice# " + invoice.InvoiceNum);
                Paragraph userPar = new Paragraph("User: " + invoice.AppUserId);
                Paragraph invoiceDatePar = new Paragraph("Invoice Date: " + invoice.InvoiceDate);
                Paragraph invoiceAmt = new Paragraph("Invoice Ammount: " + invoice.InvoiceAmt);
                Paragraph balPar = new Paragraph("Balance Due" + invoice.BalanceDue);
                doc.Open();
                doc.Add(invoiceNum);
                doc.Add(userPar);
                doc.Add(invoiceDatePar);
                doc.Add(invoiceAmt);
                doc.Add(balPar);
                doc.Close();
                wri.Close();

                //Must close doc before returning binary array from mem. 
                pdfBinary = mem.ToArray();
            }

            return File(pdfBinary, "application/pdf");
        }

        public IActionResult MaterialCertification(int appUserId)
        {
            byte[] pdfBinary; 

            using(MemoryStream mem = new MemoryStream())
            {
                Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
                PdfWriter pdfWriter = PdfWriter.GetInstance(doc, mem);
                Paragraph par = new Paragraph("Material Certificate for user: " + appUserId);
                doc.Open();
                doc.Add(par);
                doc.Close();
                pdfWriter.Close();

                //Must close doc before returning binary array from mem. 
                pdfBinary = mem.ToArray(); 
            }

            return File(pdfBinary, "application/pdf");
        }

        public IActionResult PackingSlip(int appUserId)
        {
            byte[] pdfBinary;

            using (MemoryStream mem = new MemoryStream())
            {
                Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
                PdfWriter pdfWriter = PdfWriter.GetInstance(doc, mem);
                Paragraph par = new Paragraph("Packing Slip for user: " + appUserId);
                doc.Open();
                doc.Add(par);
                doc.Close();
                pdfWriter.Close();

                //Must close doc before returning binary array from mem. 
                pdfBinary = mem.ToArray();
            }

            return File(pdfBinary, "application/pdf");
        }
    }

}
