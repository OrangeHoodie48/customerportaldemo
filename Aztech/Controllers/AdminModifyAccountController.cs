﻿using Aztech.Models;
using Aztech.Services;
using Aztech.ViewModels.CustomerModifyAccount;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Controllers
{
    [Authorize]
    public class AdminModifyAccountController : Controller
    {
        IDataServices dataServices;
        UserManager<IdentityModel> userManager;
        SignInManager<IdentityModel> signInManager;
        public AdminModifyAccountController(IDataServices dataServices, UserManager<IdentityModel> userManager, SignInManager<IdentityModel> signInManager)
        {
            this.dataServices = dataServices;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpGet]
        public async Task<IActionResult> ModifyAdminAccount()
        {
            IdentityModel iDMod = await userManager.GetUserAsync(User);
            if (iDMod.UserAuthority !=  2) return RedirectToAction("Index", "Home");

            AppUser user = dataServices.GetAppUsers().Where(r => r.AppUserId == iDMod.AppUserId).ToList()[0];
            CustomerUpdateViewModel model = new CustomerUpdateViewModel();
            model.Company = user.Company;
            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            model.Email = user.Email;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ModifyAdminAccount(CustomerUpdateViewModel inputModel)
        {
            if (!ModelState.IsValid)
            {
                return View(inputModel);
            }
            IdentityModel iDMod = await userManager.GetUserAsync(User);
            AppUser user = dataServices.GetAppUsers().Where(r => r.AppUserId == iDMod.AppUserId).ToList()[0];
            var results = await signInManager.CheckPasswordSignInAsync(iDMod, inputModel.OldPassword, false);
            if (!results.Succeeded)
            {
                ModelState.AddModelError("", "Invalid old password.");
                inputModel.OldPassword = "";
                inputModel.NewPassword = "";
                return View(inputModel);
            }


            var token = await userManager.GeneratePasswordResetTokenAsync(iDMod);
            var results2 = await userManager.ResetPasswordAsync(iDMod, token.ToString(), inputModel.NewPassword);
            if (!results2.Succeeded)
            {
                ModelState.AddModelError("", "New Password invalid or taken.");
                return View(inputModel);
            }

            iDMod.Email = inputModel.Email;
            var results3 = await userManager.UpdateAsync(iDMod);
            if (!results3.Succeeded)
            {
                throw new Exception("Problem updating Identity Model async");
            }

            user.Company = inputModel.Company;
            user.Email = inputModel.Email;
            user.FirstName = inputModel.FirstName;
            user.LastName = inputModel.LastName;
            user.Password = "";
            dataServices.UpdateAppUser(user);

            return RedirectToAction("Index", "Home");



        }
    }
}
