﻿using Aztech.Models;
using Aztech.Services;
using Aztech.ViewModels;
using Aztech.ViewModels.AddData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Controllers
{
    [Authorize]
    public class AddDataController : Controller
    {
        IDataServices dataServices;
        public AddDataController(IDataServices dataServices)
        {
            this.dataServices = dataServices;
        }

        public IActionResult AddDataPage(int appUserId)
        {
            return View(appUserId);
        }

        [HttpGet]
        public IActionResult AddOpenPartOrder(int appUserId)
        {
            OpenPartOrder order = new OpenPartOrder();
            order.AppUserId = appUserId;
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddOpenPartOrder(OpenPartOrder order)
        {
            if (!ModelState.IsValid)
            {
                return View(order);
            }
            try
            {
                Convert.ToDateTime(order.ExpectedShipDate); 
            }
            catch(FormatException ex)
            {
                ModelState.AddModelError("", "Invalid Date Format"); 
                return View(order); 
            }

            order.OrderDate = DateTime.Now.ToString();
            if (dataServices.AddOpenPartOrder(order) != true)
            {
                throw new Exception("Open Part Order Failed To Be Created In Database");
            }
            return RedirectToAction(nameof(AddDataPage), new { order.AppUserId });
        }

        [HttpGet]
        public IActionResult AddShipment(int appUserId)
        {
            if (dataServices.GetOpenPartOrdersGroupedByPO(appUserId) == null) return View("NoOpenOrders");
            AddShipmentViewModel model = new AddShipmentViewModel();
            model.AppUserId = appUserId;
            model.SeedAllPOKeys(dataServices.GetOpenPartOrdersGroupedByPO(appUserId).Keys.ToList());
            return View(model);
        }


        [HttpPost]
        public IActionResult AddShipment(AddShipmentViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.SeedAllPOKeys(dataServices.GetOpenPartOrdersGroupedByPO(model.AppUserId).Keys.ToList());
                return View(model);
            }


            Shipment shipment = new Shipment();
            shipment.AppUserId = model.AppUserId;
            shipment.Invoice = model.Invoice;
            shipment.PONumber = model.SelectedPO;
            shipment.ShipDate = model.ShipDate;
            shipment.ShipTo = model.ShipTo;
            shipment.ShipVia = model.ShipVia;
            shipment.TrackingNum = model.TrackingNum;
            

            Invoice invoice = new Invoice();
            invoice.InvoiceNum = model.Invoice;
            invoice.AppUserId = model.AppUserId;
            invoice.PONum = model.SelectedPO;
            invoice.BalanceDue = model.BalanceDue;
            invoice.InvoiceDate = model.InvoiceDate;
            invoice.InvoiceAmt = model.InvoiceAmt;
            invoice.DueDate = model.DueDate;
            invoice.TrackingNum = model.TrackingNum;
            invoice.ShipTo = model.ShipTo;
            invoice.ShipVia = model.ShipVia;

            try
            {
                Convert.ToDateTime(shipment.ShipDate);
                Convert.ToDateTime(invoice.DueDate);
                Convert.ToDateTime(invoice.InvoiceDate); 
            }
            catch(FormatException ex)
            {
                model.SeedAllPOKeys(dataServices.GetOpenPartOrdersGroupedByPO(model.AppUserId).Keys.ToList());
                ModelState.AddModelError("", "Invalid Date Fromat");
                return View(model);
            }


            int invoiceId = dataServices.AddInvoice(invoice);
            shipment.InvoiceId = invoiceId; 
            int shipmentId = dataServices.AddShipment(shipment);

            Dictionary<string, List<OpenPartOrder>> orderDictionary = dataServices.GetOpenPartOrdersGroupedByPO(model.AppUserId);

            foreach(OpenPartOrder order in orderDictionary[model.SelectedPO])
                {
                ShippedPartOrder shippedOrder = new ShippedPartOrder();
                shippedOrder.CustomPartNum = order.CustomPartNum;
                shippedOrder.AppUserId = order.AppUserId; 
                shippedOrder.Part = order.Part;
                shippedOrder.PartDescription = order.PartDescription;
                shippedOrder.PriceEach = order.PriceEach;
                shippedOrder.QtyShipped = order.QtyShipped;
                shippedOrder.ShippmentId = shipmentId;
                dataServices.AddShippedPartOrder(shippedOrder);
                dataServices.DeleteOpenPartOrder(order);
            }


            return RedirectToAction(nameof(AddDataPage), new { model.AppUserId });
        }

        [HttpGet]
        public IActionResult AddQuote(int appUserId)
        {
            Quote quote = new Quote();
            quote.AppUserId = appUserId; 
            return View(quote);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult AddQuote(Quote quote)
        {
            if (!ModelState.IsValid)
            {
                return View(quote);
            }

            try
            {
                Convert.ToDateTime(quote.OrderedDate);
            }
            catch (FormatException)
            {
                ModelState.AddModelError("", "Invalid date format");
                return View(quote);
            }

            quote.QuoteDate = DateTime.Now.ToShortDateString(); 
            dataServices.AddQuote(quote);
            return RedirectToAction(nameof(AddDataPage), new { quote.AppUserId }); 
        }
    }
}
