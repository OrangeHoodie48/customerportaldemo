﻿using Aztech.Models;
using Aztech.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Controllers
{
    [Authorize]
    public class PartsController : Controller
    {
        UserManager<IdentityModel> userManager;
        IDataServices dataServices; 
        public PartsController(UserManager<IdentityModel> userManager, IDataServices dataServices) 
        {
            this.userManager = userManager;
            this.dataServices = dataServices; 
        }

        [HttpGet]
        public async Task<IActionResult> CreatePart()
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority < 2)
            {
                return RedirectToAction("Index", "Home");
            }

            PartDTO dto = new PartDTO();
            return View(dto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreatePart(PartDTO dto)
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority < 2)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View(dto);
            }
            PartModel model = new PartModel();
            model.Name = dto.Name; 

            using(StreamReader reader = new StreamReader(dto.Table.OpenReadStream()))
            {
                model.Table = reader.ReadToEnd(); 
            }

            if (String.IsNullOrEmpty(model.Table))
            {
                return View(dto); 
            }

            dataServices.CreatePartModel(model);

            return RedirectToAction("AllParts", "Parts");
        }

        public async Task<IActionResult> AllParts()
        {
            List<PartModel> parts = await dataServices.GetParts();
            return View(parts);
        }

        public async Task<IActionResult> ViewPart(int id)
        {
            PartModel part = await dataServices.GetPart(id);
            return View(part);
        }

        public async Task<IActionResult> DeletePart(int id)
        {
            await dataServices.DeletePart(id);
            return RedirectToAction("AllParts", "Parts");
        }
    }
}



