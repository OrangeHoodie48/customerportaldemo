﻿using Aztech.Models;
using Aztech.Services;
using Aztech.ViewModels.Account;
using Aztech.ViewModels.CustomerTables;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Controllers
{

    //email: daddarios@dupage.edu password: password
    public class AccountController : Controller
    {
        UserManager<IdentityModel> userManager;
        SignInManager<IdentityModel> signInManager;
        IDataServices dataServices;
        public AccountController(UserManager<IdentityModel> userManager, SignInManager<IdentityModel> signInManager, IDataServices dataServices)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.dataServices = dataServices; 
        }

        [HttpGet]
       public async Task<IActionResult> Login()
        {
            if (userManager.Users.Where(r => r.Email == "daddarios@dupage.edu").Count() == 0)
            {
                IdentityModel iUser = new IdentityModel();
                iUser.Email = "daddarios@dupage.edu";
                iUser.UserName = iUser.Email;
                iUser.UserAuthority = 3;
                iUser.SimulationCode = -1; 
                await userManager.CreateAsync(iUser, "password");
            }

            User user = new User();
            return View(user); 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(User user)
        {
            if(!ModelState.IsValid)
            {
                return View(); 
            }
            var results = await signInManager.PasswordSignInAsync(user.Email, user.Password, true, false);
            if (results.Succeeded)
            {
                IdentityModel idMod = await userManager.FindByEmailAsync(user.Email);
                if (idMod.SimulationCode != -1)
                {
                    idMod.SimulationCode = -1;
                    var results2 = await userManager.UpdateAsync(idMod);
                    if (!results2.Succeeded) throw new Exception("Failed to update after attempting reset of simulation code on user");
                }

                if (idMod.UserAuthority < 2)
                {
                    AppUser appUser = dataServices.GetAppUsers().Where(r => r.AppUserId == idMod.AppUserId).ToList()[0];
                    if (appUser.FirstLogin.Equals("N/A"))
                    {
                        appUser.FirstLogin = DateTime.Now.ToShortDateString();
                    }

                    appUser.LastLogin = DateTime.Now.ToShortDateString();
                    dataServices.UpdateAppUser(appUser); 
                }

                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "Invalid Username or Password"); 
            return View(user); 
        }


        public async Task<IActionResult> LogOut()
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority > 1)
            {
                if(user.SimulationCode != -1)
                {
                    user.SimulationCode = -1;
                    var result = await userManager.UpdateAsync(user);
                    if (!result.Succeeded) throw new Exception("Failed to update user after attempting to reset simulation code");
                }
            }
            await signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }

        public async Task<IActionResult> ExitSimulation()
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority > 1)
            {
                if (user.SimulationCode != -1)
                {
                    user.SimulationCode = -1;
                    var result = await userManager.UpdateAsync(user);
                    if (!result.Succeeded) throw new Exception("Failed to update user after attempting to reset simulation code");
                }
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> EnterSimulation(int appUserId)
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority < 2) return RedirectToAction("Index", "Home");

            user.SimulationCode = appUserId;
            var result = await userManager.UpdateAsync(user);
            if (!result.Succeeded) throw new Exception("Failed to update user after attmepting to change simulation code");

            return RedirectToAction("Index", "Home"); 
        }

    }
}

