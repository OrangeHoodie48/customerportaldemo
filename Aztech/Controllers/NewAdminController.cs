﻿using Aztech.Models;
using Aztech.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Aztech.Controllers
{

    [Authorize]
    public class NewAdminController : Controller
    {
        IDataServices dataServices;
        PasswordGen passGen;
        UserManager<IdentityModel> userManager;
        public NewAdminController(IDataServices dataServices, UserManager<IdentityModel> userManager, PasswordGen passGen)
        {
            this.dataServices = dataServices;
            this.userManager = userManager;
            this.passGen = passGen; 
        }

        [HttpGet]
        public async Task<IActionResult> AddAdmin()
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority < 2)
            {
                return RedirectToAction("Index", "Home");
            }

            AppUser admin = new AppUser();
            admin.Type = "Admin";
            return View(admin);
        }

        [AutoValidateAntiforgeryToken]
        [HttpPost]
        public async Task<IActionResult> AddAdmin(AppUser admin)
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority < 2)
            {
                return RedirectToAction("Index", "Home");
            }
            
            admin.Type = "Admin";

            if (!ModelState.IsValid)
            {
                return View(admin);
            }
            admin.FirstLogin = "N/A";

            if (dataServices.AddAppUser(admin) != true)
            {
                throw new Exception("Failed to create customer model");
            }

            IdentityModel iUser = new IdentityModel();
            iUser.Email = admin.Email;
            iUser.AppUserId = admin.AppUserId;
            iUser.UserName = admin.Email;
            iUser.SimulationCode = -1;
            iUser.UserAuthority = 2;

            admin.Password = passGen.GetPassword();
            var result = await userManager.CreateAsync(iUser, admin.Password);

            if (!result.Succeeded)
            {
                if (dataServices.DeleteAppUser(admin) != true)
                {
                    throw new Exception("Faileed to both create Identity User and to delete admin model");
                }
                throw new Exception("Failed to create user");
            }


            string body = "Hello an account has been created for you. Use this email and the following temporary password to log in" +
                          "It is highly reccomended that you change your password upon your first log in." +
                           "   Temporary Password: " + admin.Password;
            var smtpClient = new SmtpClient
            {
                Host = "smtp.aol.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential("stevend39addario@aol.com", "C1c3r0Th3Th1rd")
            };
            using (var message = new MailMessage("stevend39addario@aol.com", admin.Email) { Subject = "Account Creation", Body = body })
            {
                smtpClient.Send(message);
            }

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> GetAdmins()
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority < 2)
            {
                return RedirectToAction("Index", "Home");
            }

            List<IdentityModel> admins = userManager.Users.Where(r => r.UserAuthority == 2).ToList();
            return View(admins); 
        }

        public async Task<IActionResult> DeleteAdmin(string id)
        {
            IdentityModel user = await userManager.GetUserAsync(User);
            if (user.UserAuthority < 2)
            {
                return RedirectToAction("Index", "Home");
            }

            IdentityModel admin = userManager.Users.FirstOrDefault(r => r.Id.Equals(id));
            await userManager.DeleteAsync(admin);
            return RedirectToAction("GetAdmins", "NewAdmin");
        }
    }
}
