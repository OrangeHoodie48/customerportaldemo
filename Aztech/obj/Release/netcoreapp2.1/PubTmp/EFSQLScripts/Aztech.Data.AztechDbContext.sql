﻿IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [AppUsers] (
        [AppUserId] int NOT NULL IDENTITY,
        [Company] nvarchar(max) NOT NULL,
        [Email] nvarchar(max) NOT NULL,
        [FirstLogin] nvarchar(max) NULL,
        [FirstName] nvarchar(max) NOT NULL,
        [LastLogin] nvarchar(max) NULL,
        [LastName] nvarchar(max) NOT NULL,
        [Password] nvarchar(max) NOT NULL,
        [RepId] nvarchar(max) NULL,
        [Type] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_AppUsers] PRIMARY KEY ([AppUserId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [AspNetRoles] (
        [Id] nvarchar(450) NOT NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [Name] nvarchar(256) NULL,
        [NormalizedName] nvarchar(256) NULL,
        CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [AspNetUsers] (
        [Id] nvarchar(450) NOT NULL,
        [AccessFailedCount] int NOT NULL,
        [AppUserId] int NOT NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [Email] nvarchar(256) NULL,
        [EmailConfirmed] bit NOT NULL,
        [LockoutEnabled] bit NOT NULL,
        [LockoutEnd] datetimeoffset NULL,
        [NormalizedEmail] nvarchar(256) NULL,
        [NormalizedUserName] nvarchar(256) NULL,
        [PasswordHash] nvarchar(max) NULL,
        [PhoneNumber] nvarchar(max) NULL,
        [PhoneNumberConfirmed] bit NOT NULL,
        [SecurityStamp] nvarchar(max) NULL,
        [SimulationCode] int NOT NULL,
        [TwoFactorEnabled] bit NOT NULL,
        [UserAuthority] int NOT NULL,
        [UserName] nvarchar(256) NULL,
        CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [Invoices] (
        [InvoiceId] int NOT NULL IDENTITY,
        [AppUserId] int NOT NULL,
        [BalanceDue] float NOT NULL,
        [DueDate] nvarchar(max) NOT NULL,
        [InvoiceAmt] float NOT NULL,
        [InvoiceDate] nvarchar(max) NOT NULL,
        [InvoiceNum] int NOT NULL,
        [PONum] nvarchar(max) NOT NULL,
        [ShipTo] nvarchar(max) NOT NULL,
        [ShipVia] nvarchar(max) NOT NULL,
        [TrackingNum] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Invoices] PRIMARY KEY ([InvoiceId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [OpenPartOrders] (
        [OpenPartOrderId] int NOT NULL IDENTITY,
        [AppUserId] int NOT NULL,
        [CustomPartNum] nvarchar(max) NOT NULL,
        [ExpectedShipDate] nvarchar(max) NOT NULL,
        [OrderDate] nvarchar(max) NULL,
        [PONum] nvarchar(max) NOT NULL,
        [Part] nvarchar(max) NOT NULL,
        [PartDescription] nvarchar(max) NOT NULL,
        [PriceEach] float NOT NULL,
        [QtyShipped] int NOT NULL,
        [SalesOrderNum] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_OpenPartOrders] PRIMARY KEY ([OpenPartOrderId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [Quotes] (
        [QuoteId] int NOT NULL IDENTITY,
        [AppUserId] int NOT NULL,
        [CustomerPartNum] nvarchar(max) NULL,
        [Notes] nvarchar(max) NULL,
        [OrderedDate] nvarchar(max) NOT NULL,
        [Part] nvarchar(max) NOT NULL,
        [PartDescription] nvarchar(max) NULL,
        [PriceEa] float NOT NULL,
        [Qty] int NOT NULL,
        [QuoteDate] nvarchar(max) NULL,
        [RFQ] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Quotes] PRIMARY KEY ([QuoteId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [Shipments] (
        [ShipmentId] int NOT NULL IDENTITY,
        [AppUserId] int NOT NULL,
        [Invoice] int NOT NULL,
        [InvoiceId] int NOT NULL,
        [PONumber] nvarchar(max) NOT NULL,
        [ShipDate] nvarchar(max) NOT NULL,
        [ShipTo] nvarchar(max) NOT NULL,
        [ShipVia] nvarchar(max) NOT NULL,
        [TrackingNum] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Shipments] PRIMARY KEY ([ShipmentId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [ShippedPartOrders] (
        [ShippedPartOrderId] int NOT NULL IDENTITY,
        [AppUserId] int NOT NULL,
        [CustomPartNum] nvarchar(max) NULL,
        [Part] nvarchar(max) NULL,
        [PartDescription] nvarchar(max) NULL,
        [PriceEach] float NOT NULL,
        [QtyShipped] int NOT NULL,
        [ShippmentId] int NOT NULL,
        CONSTRAINT [PK_ShippedPartOrders] PRIMARY KEY ([ShippedPartOrderId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [AspNetRoleClaims] (
        [Id] int NOT NULL IDENTITY,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        [RoleId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [AspNetUserClaims] (
        [Id] int NOT NULL IDENTITY,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        [UserId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [AspNetUserLogins] (
        [LoginProvider] nvarchar(450) NOT NULL,
        [ProviderKey] nvarchar(450) NOT NULL,
        [ProviderDisplayName] nvarchar(max) NULL,
        [UserId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
        CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [AspNetUserRoles] (
        [UserId] nvarchar(450) NOT NULL,
        [RoleId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
        CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE TABLE [AspNetUserTokens] (
        [UserId] nvarchar(450) NOT NULL,
        [LoginProvider] nvarchar(450) NOT NULL,
        [Name] nvarchar(450) NOT NULL,
        [Value] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
        CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190327184743_initial')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190327184743_initial', N'2.0.0-rtm-26452');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190328205638_3_28_2019_1')
BEGIN
    CREATE TABLE [PartModels] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(max) NULL,
        [Table] nvarchar(max) NULL,
        CONSTRAINT [PK_PartModels] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190328205638_3_28_2019_1')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190328205638_3_28_2019_1', N'2.0.0-rtm-26452');
END;

GO

