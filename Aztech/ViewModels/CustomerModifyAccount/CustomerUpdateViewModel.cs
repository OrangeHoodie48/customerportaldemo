﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.ViewModels.CustomerModifyAccount
{
    public class CustomerUpdateViewModel
    {
        public int AppUserId { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Company { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}
