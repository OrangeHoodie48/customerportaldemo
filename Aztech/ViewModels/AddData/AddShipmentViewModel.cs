﻿using Aztech.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.ViewModels.AddData
{
    public class AddShipmentViewModel
    {
        public List<string> AllPOKeys { get; set; }
        [Required]
        public string SelectedPO { get; set; }
        [Required]
        public int AppUserId { get; set; }
        [Required]
        public int Invoice { get; set; }
        [Required]
        public string ShipDate { get; set; }
        [Required]
        public string ShipTo { get; set; }
        [Required]
        public string ShipVia { get; set; }
        [Required]
        public string TrackingNum { get; set; }
        [Required]
        public string InvoiceDate { get; set; }
        [Required]
        public string DueDate { get; set; }
        [Required]
        public double InvoiceAmt { get; set; }
        [Required]
        public double BalanceDue { get; set; }

        public void SeedAllPOKeys(List<string> allPOKeys)
        {
            AllPOKeys = allPOKeys.OrderByDescending(r=>r).ToList();

        }
    }
}
