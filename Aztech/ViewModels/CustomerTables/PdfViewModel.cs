﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.ViewModels.CustomerTables
{
    public class PdfViewModel
    {
        public byte[] PdfBinary { get; set; }
    }
}
