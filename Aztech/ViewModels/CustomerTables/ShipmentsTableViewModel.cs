﻿using Aztech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.ViewModels.CustomerTables
{
    public class ShipmentsTableViewModel
    {
        public int AppUserId { get; set; }
        public List<Shipment> Shipments { get; set; }
        public Dictionary<int, List<ShippedPartOrder>> ShippedPartsByShipmentId { get; set; }
    }
}
