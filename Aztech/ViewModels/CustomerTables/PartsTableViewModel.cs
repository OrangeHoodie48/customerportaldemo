﻿using Aztech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.ViewModels.CustomerTables
{
    public class PartsTableViewModel
    {
        public List<Part> parts { get; set; }
        public int AppUserId { get; set; }
    }
}
