﻿using Aztech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.ViewModels.CustomerTables
{
    public class QuotesTableViewModel
    {
        public List<Quote> Quotes { get; set; }
        public int AppUserId { get; set; }
    }
}
