﻿using Aztech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.ViewModels.CustomerTables
{
    public class OpenOrdersTableViewModel
    {
        public AppUser CustomerProfile { get; set; }
        public List<OpenPartOrder> AllOrders { get; set; }
    }
}
