﻿using Aztech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.ViewModels.CustomerTables
{
    public class InvoicesTableViewModel
    {
        public int AppUserId { get; set; }
        public List<Invoice> Invoices { get; set; }
    }
}
