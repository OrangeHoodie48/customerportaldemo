﻿using Microsoft.AspNetCore.Builder.Internal;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.AspNetCore.Builder
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseNodeModules(this IApplicationBuilder app, string root)
        {
            string path = Path.Combine(root, "node_modules");
            StaticFileOptions options = new StaticFileOptions();
            options.FileProvider = new PhysicalFileProvider(path);
            options.RequestPath = "/node_modules";

            app.UseStaticFiles(options);
            return app; 
        }
    }
}
