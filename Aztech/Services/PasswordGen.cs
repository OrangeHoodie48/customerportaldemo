﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Services
{
    public class PasswordGen
    {
        Random rand; 
        public PasswordGen()
        {
            rand = new Random(); 
        }
        public string GetPassword(int passLength = 10)
        {
            string pass = ""; 

            for(int i = 0; i < passLength; i++)
            {
                pass += Math.Floor(rand.NextDouble() * 10); 
            }

            return pass; 
        }
    }
}
