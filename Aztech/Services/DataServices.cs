﻿using Aztech.Data;
using Aztech.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Services
{
    public class DataServices : IDataServices
    {
        AztechDbContext context;
        public DataServices(AztechDbContext context)
        {
            this.context = context;
        }

        public bool AddAppUser(AppUser user)
        {
            if (context.AppUsers.FirstOrDefault(r => r.AppUserId == user.AppUserId) != null)
            {
                return false;
            }

            context.AppUsers.Add(user);
            context.SaveChanges();
            return true;
        }

        public List<AppUser> GetAppUsers()
        {
            return context.AppUsers.ToList();
        }

        public bool DeleteAppUser(AppUser user)
        {
            user = context.AppUsers.FirstOrDefault(r => r.AppUserId == user.AppUserId);
            if (user != null)
            {
                context.AppUsers.Remove(user);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateAppUser(AppUser user)
        {
            context.AppUsers.Attach(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
            return true;

        }

        public bool AddOpenPartOrder(OpenPartOrder order)
        {
            context.OpenPartOrders.Add(order);
            context.SaveChanges();
            return true;
        }

        public bool DeleteOpenPartOrder(OpenPartOrder order)
        {
            if (context.OpenPartOrders.FirstOrDefault(r => r.OpenPartOrderId == order.OpenPartOrderId) != null)
            {
                context.OpenPartOrders.Remove(order);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public OpenPartOrder GetOpenPartOrder(int openPartId)
        {
            OpenPartOrder order = context.OpenPartOrders.FirstOrDefault(r => r.OpenPartOrderId == openPartId);
            return order;
        }

        public bool AddShippedPartOrder(ShippedPartOrder order)
        {
            context.ShippedPartOrders.Add(order);
            context.SaveChanges();
            return true;
        }
        public int AddShipment(Shipment shipment)
        {
            context.Shipments.Add(shipment);
            context.SaveChanges();
            return shipment.ShipmentId;
        }

        public List<ShippedPartOrder> GetShippedPartOrdersByUser(int appUserId)
        {
            return context.ShippedPartOrders.Where(r => r.AppUserId == appUserId).ToList();
        }

        public List<OpenPartOrder> GetCustomerOpenPartOrders(int appUserId)
        {
            List<OpenPartOrder> orders = context.OpenPartOrders.Where(r => r.AppUserId == appUserId).ToList();
            return orders;
        }
        public List<Shipment> GetCustomerShipments(int appUserId)
        {
            List<Shipment> shipments = context.Shipments.Where(r => r.AppUserId == appUserId).ToList();
            return shipments;
        }
        public List<ShippedPartOrder> GetShippedPartOrders(int shipmentId)
        {
            List<ShippedPartOrder> shippedPartOrders = context.ShippedPartOrders.Where(r => r.ShippmentId == shipmentId).ToList();
            return shippedPartOrders;
        }
        public Dictionary<string, List<OpenPartOrder>> GetOpenPartOrdersGroupedByPO(int appUserId)
        {
            Dictionary<string, List<OpenPartOrder>> orders = new Dictionary<string, List<OpenPartOrder>>();
            List<OpenPartOrder> allCustomerOrders = context.OpenPartOrders.Where(r => r.AppUserId == appUserId).ToList();
            if (allCustomerOrders.Count == 0) return null;
            foreach (OpenPartOrder order in allCustomerOrders)
            {
                if (orders.Keys.Count == 0 || !orders.Keys.Contains(order.PONum))
                {
                    orders[order.PONum] = new List<OpenPartOrder>();
                    orders[order.PONum].Add(order);
                }
                else
                {
                    orders[order.PONum].Add(order);
                }

            }
            return orders;
        }

        public Dictionary<int, List<ShippedPartOrder>> GetShippedPartsGroupedByShipment(int appUserId)
        {
            Dictionary<int, List<ShippedPartOrder>> partsToShipment = new Dictionary<int, List<ShippedPartOrder>>();

            List<Shipment> shipments = GetCustomerShipments(appUserId);
            List<ShippedPartOrder> parts;
            foreach (Shipment shipment in shipments)
            {
                partsToShipment.Add(shipment.ShipmentId, new List<ShippedPartOrder>());
                parts = GetShippedPartOrders(shipment.ShipmentId);
                foreach (ShippedPartOrder part in parts)
                {
                    partsToShipment[shipment.ShipmentId].Add(part);
                }
            }

            return partsToShipment;
        }

        public int AddInvoice(Invoice model)
        {
            context.Invoices.Add(model);
            context.SaveChanges();
            return model.InvoiceId;
        }
        public List<Invoice> GetInvoicesFor(int appUserId)
        {
            return context.Invoices.Where(r => r.AppUserId == appUserId).ToList();
        }
        public Shipment GetShipment(int shipmentId)
        {
            return context.Shipments.Where(r => r.ShipmentId == shipmentId).ToList()[0];
        }

        public List<Part> GetPartsForUser(int appUserId)
        {
            List<Part> parts = new List<Part>();
            List<ShippedPartOrder> orders = GetShippedPartOrdersByUser(appUserId);
            Dictionary<string, List<ShippedPartOrder>> ordersByPartName = new Dictionary<string, List<ShippedPartOrder>>();
            Part part;
            foreach (ShippedPartOrder order in orders)
            {
                if (!ordersByPartName.Keys.Contains(order.Part))
                {
                    ordersByPartName[order.Part] = new List<ShippedPartOrder>();
                    ordersByPartName[order.Part].Add(order);
                }
                else
                {
                    ordersByPartName[order.Part].Add(order);
                }
            }

            List<ShippedPartOrder> shippedPartOrders;
            List<string> keys = ordersByPartName.Keys.ToList();
            Shipment shipment;
            string lastPO = "";
            DateTime shipDate = Convert.ToDateTime("1/1/1975");
            foreach (string key in keys)
            {
                shippedPartOrders = ordersByPartName[key];
                part = new Part();
                part.AppUserId = appUserId;
                part.PartName = key;
                part.PartDescription = shippedPartOrders[0].PartDescription;
                part.CustomerNum = shippedPartOrders[0].CustomPartNum;

                for (int i = 0; i < shippedPartOrders.Count; i++)
                {
                    shipment = GetShipment(shippedPartOrders[i].ShippmentId);
                    if (i == 0)
                    {
                        shipDate = Convert.ToDateTime(shipment.ShipDate);
                        lastPO = shipment.PONumber;
                    }
                    else if (Convert.ToDateTime(shipment.ShipDate).Ticks > shipDate.Ticks)
                    {
                        shipDate = Convert.ToDateTime(shipment.ShipDate);
                        lastPO = shipment.PONumber;
                    }
                }

                part.LastShipDate = shipDate.ToShortDateString();
                part.LastPO = lastPO;
                parts.Add(part);
            }
            return parts;
        }

        public bool AddQuote(Quote quote)
        {
            context.Quotes.Add(quote);
            context.SaveChanges();
            return true;
        }

        public List<Quote> GetQuotesForUser(int appUserId)
        {
            return context.Quotes.Where(r => r.AppUserId == appUserId).ToList();
        }

        public Invoice GetInvoice(int invoiceId)
        {
            return context.Invoices.FirstOrDefault(r => r.InvoiceId == invoiceId);
        }

        public PartModel CreatePartModel(PartModel part)
        {
            context.PartModels.Add(part);
            context.SaveChanges();
            return part;
        }

        public bool DeletePartModel(int id)
        {
            PartModel part = context.PartModels.FirstOrDefault(r => r.Id == id);
            if (part == null) throw new Exception("Tried to delete a PartModel that doesn't exist");
            context.PartModels.Remove(part);
            return true;
        }

        public async Task<List<PartModel>> GetParts()
        {
            return await context.PartModels.OrderBy(r => r.Name).ToListAsync();
        }


        public async Task<PartModel> GetPart(int id)
        {
            return await context.PartModels.FirstOrDefaultAsync(r => r.Id == id);
        }

        public async Task<PartModel> DeletePart(int id)
        {
            PartModel part = await GetPart(id);
            context.PartModels.Remove(part);
            await context.SaveChangesAsync();
            return part;
        }


    }
}
