﻿using Aztech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Services
{
    public interface IDataServices
    {
        bool AddAppUser(AppUser user);
        List<AppUser> GetAppUsers();
        bool DeleteAppUser(AppUser user);
        bool UpdateAppUser(AppUser user);
        bool AddOpenPartOrder(OpenPartOrder order);
        bool DeleteOpenPartOrder(OpenPartOrder order);
        bool AddShippedPartOrder(ShippedPartOrder order);
        int AddShipment(Shipment shipment);
        Shipment GetShipment(int shipmentId); 
        OpenPartOrder GetOpenPartOrder(int openPartId); 
        List<OpenPartOrder> GetCustomerOpenPartOrders(int appUserId);
        List<Shipment> GetCustomerShipments(int appUserId);
        List<ShippedPartOrder> GetShippedPartOrders(int shipmentId);
        Dictionary<string, List<OpenPartOrder>> GetOpenPartOrdersGroupedByPO(int appUserId);
        Dictionary<int, List<ShippedPartOrder>> GetShippedPartsGroupedByShipment(int appUserId);
        int AddInvoice(Invoice model);
        List<Invoice> GetInvoicesFor(int appUserId);
        Invoice GetInvoice(int invoiceId);
        List<Part> GetPartsForUser(int appUserId);
        List<ShippedPartOrder> GetShippedPartOrdersByUser(int appUserId);
        bool AddQuote(Quote quote);
        List<Quote> GetQuotesForUser(int appUserId);
        PartModel CreatePartModel(PartModel part);
        bool DeletePartModel(int id);
        Task<List<PartModel>> GetParts();
        Task<PartModel> GetPart(int id);
        Task<PartModel> DeletePart(int id);
    }
}
