﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Models
{
    public class ShippedPartOrder
    {
        public int ShippedPartOrderId { get; set; }
        public int AppUserId { get; set; }
        public int ShippmentId { get; set; }
        public string CustomPartNum { get; set; }
        public string Part { get; set; }
        public string PartDescription { get; set; }
        public int QtyShipped { get; set; }
        public double PriceEach { get; set; }
    }
}
