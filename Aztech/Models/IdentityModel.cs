﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Models
{
    public class IdentityModel : IdentityUser 
    {
        public int UserAuthority { get; set; }
        public int SimulationCode { get; set; }
        public int AppUserId { get; set; }
    }
}
