﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Models
{
    public class Part
    {
        public int AppUserId { get; set; }
        public string PartName { get; set; }
        public string CustomerNum { get; set; }
        public string PartDescription { get; set; }
        public string LastShipDate { get; set; }
        public string LastPO { get; set; }
    }
}
