﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Models
{
    public class Invoice
    {
        public int InvoiceId { get; set; }
        [Required]
        public int InvoiceNum { get; set; }
        [Required]
        public int AppUserId { get; set; }
        [Required]
        public string PONum { get; set; }
        [Required]
        public string InvoiceDate { get; set; }
        [Required]
        public string DueDate { get; set; }
        [Required]
        public double InvoiceAmt { get; set; }
        [Required]
        public double BalanceDue { get; set; }
        [Required]
        public string ShipTo { get; set; }
        [Required]
        public string ShipVia { get; set; }
        [Required]
        public string TrackingNum { get; set; }
    }
}
