﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Models
{
    public class AppUser
    {
        [Key]
        public int AppUserId { get; set; }
        public string RepId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Company { get; set; }
        [Required]
        public string Email { get; set; }
        public string FirstLogin { get; set; }
        public string LastLogin { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
