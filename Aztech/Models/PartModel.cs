﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Models
{
    public class PartModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Table { get; set; }

        public string[][] GetTableArray()
        {
            string[] str1 = Table.Split("\n");
            string[][] tableArray = new string[str1.Length][];

            for (int i = 0; i < str1.Length; i++)
            {
                tableArray[i] = str1[i].Split(","); 
            }

            return tableArray;
        }
    }
}
