﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Models
{
    public class Shipment
    {
        public int ShipmentId { get; set; }
        [Required]
        public int AppUserId { get; set; }
        [Required]
        public string PONumber { get; set; }
        [Required]
        public int Invoice { get; set; }
        [Required]
        public string ShipDate { get; set; }
        [Required]
        public string ShipTo { get; set; }
        [Required]
        public string ShipVia { get; set; }
        [Required]
        public string TrackingNum { get; set; }
        public int InvoiceId { get; set; }
        
    }
}

