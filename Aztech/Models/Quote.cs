﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Models
{
    public class Quote
    {
        public int QuoteId { get; set; }
        public int AppUserId { get; set; }
        [Required]
        public string Part { get; set; }
        public string PartDescription { get; set; }
        public string CustomerPartNum { get; set; }
        [Required]
        public int Qty { get; set; }
        [Required]
        public string RFQ { get; set; }
        [Required]
        public string OrderedDate { get; set; }
        public string QuoteDate { get; set; }
        [Required]
        public double PriceEa { get; set; }
        public string Notes { get; set; }

    }
}
