﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Aztech.Models
{
    public class OpenPartOrder
    {
        public int OpenPartOrderId { get; set; }
        [Required]
        public string PONum { get; set; }
        [Required]
        public string SalesOrderNum { get; set; }
        [Required]
        public string ExpectedShipDate { get; set; }
        public string OrderDate { get; set; }
        [Required]
        public int AppUserId { get; set; }
        [Required]
        public string CustomPartNum { get; set; }
        [Required]
        public string Part { get; set; }
        [Required]
        public string PartDescription { get; set; }
        [Required]
        public int QtyShipped { get; set; }
        [Required]
        public double PriceEach { get; set; }
        
    }
}
